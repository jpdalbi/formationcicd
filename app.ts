import { Backend, KuzzleRequest } from 'kuzzle'

const app = new Backend('app')
app.controller.register('greeting', {
  actions: {
    sayHello: {
      handler: async (request: KuzzleRequest) => {
        return `Hello, ${request.getString('name')}`;
      },
      http: [
        // generated route: "GET http://<host>:<port>/greeting/hello"
        { verb: 'get', path: '/greeting/hello' },
        // generated route: "POST http://<host>:<port>/_/hello/world"
        { verb: 'post', path: 'hello/world' },
      ]
    }
  }
});

app.start()
  .then(() => {
    app.log.info('Application started')
  })
  .catch(console.error)
