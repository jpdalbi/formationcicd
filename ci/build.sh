#!/usr/bin/env sh
#==============================================================================
# Each Gitlab CI pipeline is refer to a Git commit
# This job build a commit specific Docker image
#==============================================================================

set -e

DOCKER_IMAGE="$CI_REGISTRY_IMAGE/kuzzle:$CI_COMMIT_SHORT_SHA"

# Build
# -----------------------------------------------------------------------------
echo "=== Building Kuzzle Docker image with tag $DOCKER_IMAGE"
docker build -q --pull \
  -t "$DOCKER_IMAGE" \
  -f Dockerfile \
  .

# Publish Docker image on Gitlab project registry
# -----------------------------------------------------------------------------
echo "=== Pushing Kuzzle Docker image with tag $DOCKER_IMAGE"
docker push "$DOCKER_IMAGE"
