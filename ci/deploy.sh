#!/usr/bin/env sh
#==============================================================================
# Each Gitlab CI pipeline is refer to a Git commit
# This job build a commit specific Docker image
#==============================================================================

set -e

curl https://baltocdn.com/helm/signing.asc | apt-key add - \
 && apt-get install apt-transport-https --yes \
 && echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list \
 && apt-get update \
 && apt-get install -y helm

DOCKER_IMAGE="$CI_REGISTRY_IMAGE/kuzzle:$CI_COMMIT_SHORT_SHA"
RELEASE_IMAGE="$CI_REGISTRY_IMAGE/kuzzle:latest"


echo ${KUBE_CONFIG} | base64 -d > kube_config.yml && chmod 600 ./kube_config.yml
if helm upgrade kuzzle ./ci/charts/kuzzle --kubeconfig ./kube_config.yml --values ./ci/helm_values/kuzzle.yaml --set image.name=$CI_REGISTRY_IMAGE/kuzzle --set image.tag="$CI_COMMIT_SHORT_SHA" --wait --timeout 5m; then
  # Retag if deploy succeeded
  # -----------------------------------------------------------------------------
  echo "=== Taging Docker image with release tag $RELEASE_IMAGE"
  docker pull "$DOCKER_IMAGE"
  docker tag "$DOCKER_IMAGE" "$RELEASE_IMAGE"

  # Publish Docker image
  # -----------------------------------------------------------------------------
  echo "=== Pushing Kuzzle Docker image with tag $DOCKER_IMAGE"
  docker push "$RELEASE_IMAGE"
else
  helm rollback kuzzle --kubeconfig ./kube_config.yml
fi
